import { Env, parse } from "./parse.ts";
import { AtLeastOne, envPlan, EnvType, loadConfig } from "./types/mod.ts";
import { verify } from "./verify.ts";

export async function load<T extends envPlan>(values: T, config?: AtLeastOne<loadConfig>): Promise<EnvType<T>> {
  const parsed = await parse(config?.path ?? ".env");

  let merged: Env;
  if (config?.merge) {
    if (config.mergePriority == ("process" || undefined))
      merged = {
        ...parsed,
        ...Deno.env.toObject(),
      };
    else
      merged = {
        ...Deno.env.toObject(),
        ...parsed,
      };
  } else merged = parsed;

  if (config?.export !== false) {
    for (const [key, value] of Object.values(merged ?? parsed)) {
      if (typeof value !== "string") continue;
      Deno.env.set(key, value);
    }
  }

  const verified = verify(merged ?? parsed, values);

  return verified as EnvType<T>;
}
