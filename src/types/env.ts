export type finalEnv = {
  [index: string]: boolean | Uint8Array | number | string | RegExp | Array<string> | bigint;
};

export interface loadConfig {
  /** Path to .env file is. Defaults to `"folder you are running deno command from"/.env` */
  path?: string;
  /** Should .env values be set to Deno.env ?
   *
   * @default true
   */
  export?: boolean;

  /** Merge .env variables with process variables? */
  merge?: boolean;

  /** Determines if .env file or process env should have a priority.
   * (The one with priority overrides existing variables.)
   *
   * @default "process"
   */
  mergePriority?: ".env" | "process";
}

export type envPlan = {
  [key: string]: envPlanValue;
};

// deno-lint-ignore no-explicit-any
export type customEnv<T extends any = any> = (val?: string) => T;

export type envPlanType =
  | BooleanConstructor
  | Uint8ArrayConstructor
  | NumberConstructor
  | StringConstructor
  | RegExp
  | readonly string[]
  | BigIntConstructor
  | customEnv;

export type envPlanValue = envPlanType | envPlanDefaultValue | envPlanOptionalValue;

export type envPlanDefaultValue<T extends envPlanType = envPlanType> = {
  type: T;
  default: T extends BooleanConstructor
    ? boolean
    : T extends Uint8ArrayConstructor
    ? Uint8Array
    : T extends NumberConstructor
    ? number
    : T extends StringConstructor
    ? string
    : T extends RegExp
    ? RegExp
    : T extends readonly string[]
    ? T[number]
    : T extends BigIntConstructor
    ? bigint
    : never;
};

export type envPlanOptionalValue<T extends envPlanType = envPlanType> = {
  type: T;
  optional: boolean;
};

export type envRes<T extends envPlanType> = T extends BooleanConstructor
  ? boolean
  : T extends Uint8Array
  ? Uint8Array
  : T extends NumberConstructor
  ? number
  : T extends StringConstructor
  ? string
  : T extends RegExp
  ? string
  : T extends readonly string[]
  ? T[number]
  : T extends BigIntConstructor
  ? bigint
  : never;

export type EnvType<S extends envPlan> = {
  [K in keyof S]: S[K] extends customEnv
    ? ReturnType<S[K]>
    : S[K] extends envPlanDefaultValue
    ? envRes<S[K]["type"]>
    : S[K] extends envPlanOptionalValue<envPlanType>
    ? S[K]["optional"] extends true
      ? envRes<S[K]["type"]> | undefined
      : envRes<S[K]["type"]>
    : S[K] extends envPlanType
    ? envRes<S[K]>
    : never;
};
