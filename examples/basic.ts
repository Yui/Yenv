import { load } from "../mod.ts";

const env = await load({
  link: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,
});

console.log(env);
